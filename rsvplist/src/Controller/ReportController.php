<?php
/**
 * @file
 * Contains \Drupal\rsvplist\Controller\ReportController
 */

namespace Drupal\rsvplist\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use PDO;

/**
 * Controller for RSVP List Report
 */
class ReportController extends ControllerBase {

  /**
   * @return array
   */
  public function report() {
    $content = [];
    $content['message'] = [
      '#markup' => $this->t('Below is a list of all Events RSVPs including username,email
      adress and the name of thee event they will be attending.'),
    ];
    $headers = [
      $this->t('Name'),
      $this->t('Event'),
      $this->t('Email'),
    ];

    $rows = [];
    foreach
    ($this->load() as $entry) {
      $obj_html = new Html;
      // Sanitize each entry.
      $rows[] = array_map(function ($entry) use ($obj_html) {
        return $obj_html::escape($entry);
      }, $entry);

    }
    $content['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No entries available'),
    ];
    $content['#cache']['max-age'] = 0;
    return $content;
  }

  /**
   * @return array
   */
  protected function load() {
    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->join('users_field_data', 'u', 'r.uid = u.uid');
    $select->join('node_field_data', 'n', 'r.nid = n.nid');
    $select->addField('u', 'name', 'username');
    $select->addField('n', 'title');
    $select->addField('r', 'mail');
    return $select->execute()->fetchAll(PDO::FETCH_ASSOC);
  }


}
