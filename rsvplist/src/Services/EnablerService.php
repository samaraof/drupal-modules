<?php
/**
 * @file
 * Contains \Drupal\rsvplist\Services\EnablerService
 */

namespace Drupal\rsvplist\Services;

use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;

/**
 * Defines a servicee for managing RSVP list enabled for nodes
 */
class EnablerService {

  /**
   * Constructor
   */
  public function __construct() {

  }

  /**
   * @param \Drupal\node\Entity\Node $node
   *
   * @return void
   * @throws \Exception
   */
  public function setEnabled(Node $node) {
    if (!$this->isEnabled($node)) {
      $insert = Database::getConnection()->insert('rsvplist_enabled');
      $insert->fields(['nid'], [$node->id()]);
      $insert->execute();
    }
  }

  /**
   * @param \Drupal\node\Entity\Node $node
   *
   * @return bool
   */
  public function isEnabled(Node $node): bool {
    if ($node->isNew()) {
      return FALSE;
    }
    $select = Database::getConnection()->select('rsvplist_enabled', 're');
    $select->fields('re', ['nid']);
    $select->condition('nid', $node->id());
    $results = $select->execute();
    return !empty($results->fetchCol());
  }

  /**
   * @param \Drupal\node\Entity\Node $node
   *
   * @return void
   */
  public function delEnabled(Node $node) {
    $delete = Database::getConnection()->delete('rsvplist_enabled');
    $delete->condition('nid', $node->id());
    $delete->execute();
  }

}
